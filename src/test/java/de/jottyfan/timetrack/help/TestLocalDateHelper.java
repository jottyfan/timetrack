package de.jottyfan.timetrack.help;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.junit.jupiter.api.Test;

/**
 * 
 * @author henkej
 *
 */
public class TestLocalDateHelper {
	@Test
	public void testFindWeekDays() {
		assertEquals(20, LocalDateHelper.seekDay(LocalDate.of(2022, 2, 23), DayOfWeek.SUNDAY, true).getDayOfMonth());
		assertEquals(26, LocalDateHelper.seekDay(LocalDate.of(2022, 2, 23), DayOfWeek.SATURDAY, false).getDayOfMonth());
		assertEquals(13, LocalDateHelper.seekDay(LocalDate.of(2022, 2, 19), DayOfWeek.SUNDAY, true).getDayOfMonth());
		assertEquals(19, LocalDateHelper.seekDay(LocalDate.of(2022, 2, 19), DayOfWeek.SATURDAY, false).getDayOfMonth());
		assertEquals("26.12.2021", LocalDateHelper.seekDay(LocalDate.of(2022, 1, 1), DayOfWeek.SUNDAY, true).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
		assertEquals("02.01.2022", LocalDateHelper.seekDay(LocalDate.of(2021, 12, 31), DayOfWeek.SUNDAY, false).format(DateTimeFormatter.ofPattern("dd.MM.yyyy")));
	}
}

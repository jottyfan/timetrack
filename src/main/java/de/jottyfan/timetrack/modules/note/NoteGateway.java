package de.jottyfan.timetrack.modules.note;

import static de.jottyfan.timetrack.db.note.Tables.T_NOTE;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertValuesStep4;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.timetrack.db.note.enums.EnumCategory;
import de.jottyfan.timetrack.db.note.enums.EnumNotetype;
import de.jottyfan.timetrack.db.note.tables.records.TNoteRecord;

/**
 * 
 * @author jotty
 *
 */
@Repository
public class NoteGateway {
	private static final Logger LOGGER = LogManager.getLogger(NoteGateway.class);
	private final DSLContext jooq;

	public NoteGateway(@Autowired DSLContext jooq) throws Exception {
		this.jooq = jooq;
	}

	public DSLContext getJooq() {
		return this.jooq;
	}

	/**
	 * get sorted list of notes
	 * 
	 * @return a list (an empty one at least)
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public List<NoteBean> getAll() throws DataAccessException, ClassNotFoundException, SQLException {
		SelectJoinStep<Record5<Integer, String, EnumCategory, String, EnumNotetype>> sql = getJooq()
		// @formatter:off
			.select(T_NOTE.PK,
					    T_NOTE.TITLE,
					    T_NOTE.CATEGORY,
					    T_NOTE.CONTENT,
					    T_NOTE.NOTETYPE)
			.from(T_NOTE);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		List<NoteBean> list = new ArrayList<>();
		for (Record5<Integer, String, EnumCategory, String, EnumNotetype> r : sql.fetch()) {
			NoteBean bean = new NoteBean(r.get(T_NOTE.PK));
			bean.setTitle(r.get(T_NOTE.TITLE));
			bean.setCategory(r.get(T_NOTE.CATEGORY));
			bean.setContent(r.get(T_NOTE.CONTENT));
			bean.setType(r.get(T_NOTE.NOTETYPE));
			list.add(bean);
		}
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	/**
	 * delete a note from the database
	 * 
	 * @param pk the id of the contact
	 * @return the number of affected database rows, should be 1
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer delete(Integer pk) throws DataAccessException, ClassNotFoundException, SQLException {
		DeleteConditionStep<TNoteRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_NOTE)
			.where(T_NOTE.PK.eq(pk));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * add a note to the database
	 *
	 * @param bean the contact information
	 * @return the number of affected database rows, should be 1
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer add(NoteBean bean) throws DataAccessException, ClassNotFoundException, SQLException {
		InsertValuesStep4<TNoteRecord, String, EnumCategory, String, EnumNotetype> sql = getJooq()
		// @formatter:off
			.insertInto(T_NOTE,
					        T_NOTE.TITLE,
									T_NOTE.CATEGORY,
									T_NOTE.CONTENT,
									T_NOTE.NOTETYPE)
			.values(bean.getTitle(), bean.getCategory(), bean.getContent(), bean.getType());
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * update a note in the database, referencing its pk
	 * 
	 * @param bean the contact information
	 * @return the number of affected database rows, should be 1
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer update(NoteBean bean) throws DataAccessException, ClassNotFoundException, SQLException {
		UpdateConditionStep<TNoteRecord> sql = getJooq()
		// @formatter:off
			.update(T_NOTE)
			.set(T_NOTE.TITLE, bean.getTitle())
			.set(T_NOTE.CATEGORY, bean.getCategory())
			.set(T_NOTE.CONTENT, bean.getContent())
			.set(T_NOTE.NOTETYPE, bean.getType())
			.where(T_NOTE.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * get number of entries in t_note
	 * 
	 * @return number of entries
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer getAmount() throws DataAccessException, ClassNotFoundException, SQLException {
		SelectJoinStep<Record1<Integer>> sql = getJooq()
		// @formatter:off
			.selectCount()
			.from(T_NOTE);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.fetchOne(DSL.count());
	}

	/**
	 * get all enum types
	 * 
	 * @return list of enum types
	 */
	public List<EnumNotetype> getTypes() {
		return new ArrayList<>(Arrays.asList(EnumNotetype.values()));
	}

	public NoteBean getBean(Integer id) {
		SelectConditionStep<Record5<Integer, String, EnumCategory, String, EnumNotetype>> sql = getJooq()
		// @formatter:off
			.select(T_NOTE.PK,
					    T_NOTE.TITLE,
					    T_NOTE.CATEGORY,
					    T_NOTE.CONTENT,
					    T_NOTE.NOTETYPE)
			.from(T_NOTE)
			.where(T_NOTE.PK.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		for (Record5<Integer, String, EnumCategory, String, EnumNotetype> r : sql.fetch()) {
			NoteBean bean = new NoteBean(r.get(T_NOTE.PK));
			bean.setTitle(r.get(T_NOTE.TITLE));
			bean.setCategory(r.get(T_NOTE.CATEGORY));
			bean.setContent(r.get(T_NOTE.CONTENT));
			bean.setType(r.get(T_NOTE.NOTETYPE));
			return bean;
		}
		return null;
	}
}

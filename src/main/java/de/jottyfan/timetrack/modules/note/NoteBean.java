package de.jottyfan.timetrack.modules.note;

import java.io.Serializable;

import de.jottyfan.timetrack.db.note.enums.EnumCategory;
import de.jottyfan.timetrack.db.note.enums.EnumNotetype;

/**
 * 
 * @author henkej
 *
 */
public class NoteBean implements Serializable, Comparable<NoteBean> {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String title;
	private EnumCategory category;
	private String content;
	private EnumNotetype type;

	public NoteBean() {
		super();
		this.pk = null;
	}

	public NoteBean(Integer pk) {
		super();
		this.pk = pk;
	}

	@Override
	public int compareTo(NoteBean o) {
		return o == null ? 0 : getPk().compareTo(o.getPk());
	}

	/**
	 * @return the pk
	 */
	public Integer getPk() {
		return pk;
	}

	/**
	 * @param pk the pk to set
	 */
	public void setPk(Integer pk) {
		this.pk = pk;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the category
	 */
	public EnumCategory getCategory() {
		return category;
	}

	/**
	 * @param category the category to set
	 */
	public void setCategory(EnumCategory category) {
		this.category = category;
	}

	/**
	 * @return the content
	 */
	public String getContent() {
		return content;
	}

	/**
	 * @param content the content to set
	 */
	public void setContent(String content) {
		this.content = content;
	}

	/**
	 * @return the type
	 */
	public EnumNotetype getType() {
		return type;
	}

	/**
	 * @param type the type to set
	 */
	public void setType(EnumNotetype type) {
		this.type = type;
	}
}

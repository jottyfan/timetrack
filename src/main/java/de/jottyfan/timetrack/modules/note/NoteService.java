package de.jottyfan.timetrack.modules.note;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 * 
 * @author henkej
 *
 */
@Service
@Transactional(transactionManager = "transactionManager")
public class NoteService {
	private static final Logger LOGGER = LogManager.getLogger(NoteService.class);

	@Autowired
	private DSLContext dsl;
	
	@Autowired
	private OAuth2AuthorizedClientService security;

	public String getCurrentUser(OAuth2AuthenticationToken token) {
		if (token != null) {
			OAuth2AuthorizedClient client = security.loadAuthorizedClient(token.getAuthorizedClientRegistrationId(),
					token.getName());
			if (client != null) {
				return client.getPrincipalName();
			} else {
				return "client is null";
			}
		} else {
			return "oauth token is null";
		}
	}

	public List<NoteBean> getList() {
		try {
			return new NoteGateway(dsl).getAll();
		} catch (Exception e) {
			LOGGER.error(e);
			return new ArrayList<>();
		}
	}

	public Integer doUpsert(NoteBean bean) {
		try {
			NoteGateway gw = new NoteGateway(dsl);
			return bean.getPk() == null ? gw.add(bean) : gw.update(bean);
		} catch (Exception e) {
			LOGGER.error(e);
			return 0;
		}
	}

	public Integer doDelete(Integer pk) {
		try {
			return new NoteGateway(dsl).delete(pk);
		} catch (Exception e) {
			LOGGER.error(e);
			return 0;
		}
	}

	public Integer getAmount() {
		return getList().size();
	}

	public NoteBean getBean(Integer id) {
		try {
			return new NoteGateway(dsl).getBean(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}
}

package de.jottyfan.timetrack.modules.note;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.jottyfan.timetrack.component.OAuth2Provider;
import de.jottyfan.timetrack.db.note.enums.EnumCategory;
import de.jottyfan.timetrack.db.note.enums.EnumNotetype;
import de.jottyfan.timetrack.modules.profile.ProfileService;
import jakarta.annotation.security.RolesAllowed;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class NoteController {

	@Autowired
	private OAuth2Provider provider;

	@Autowired
	private ProfileService profileService;

	@Autowired
	private NoteService noteService;

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/note/list")
	public String getList(Model model) {
		String username = provider.getName();
		List<NoteBean> list = noteService.getList();
		model.addAttribute("noteList", list);
		model.addAttribute("theme", profileService.getTheme(username));
		return "note/list";
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/note/add", method = RequestMethod.GET)
	public String toAdd(Model model, OAuth2AuthenticationToken token) {
		return toItem(null, model);
	}

	@RolesAllowed("timetrack_user")
  @GetMapping("/note/edit/{id}")
	public String toItem(@PathVariable Integer id, Model model) {
		String username = provider.getName();
		NoteBean bean = noteService.getBean(id);
		if (bean == null) {
			bean = new NoteBean(); // the add case
		}
		model.addAttribute("noteBean", bean);
		model.addAttribute("types", Arrays.asList(EnumNotetype.values()));
		model.addAttribute("categories", Arrays.asList(EnumCategory.values()));
		model.addAttribute("theme", profileService.getTheme(username));
		return "note/item";
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/note/upsert", method = RequestMethod.POST)
	public String doUpsert(Model model, @ModelAttribute NoteBean bean, OAuth2AuthenticationToken token) {
		Integer amount = noteService.doUpsert(bean);
		return amount.equals(1) ? getList(model) : toItem(bean.getPk(), model);
	}

	@RolesAllowed("timetrack_user")
	@GetMapping(value = "/note/delete/{id}")
	public String doDelete(@PathVariable Integer id, Model model, OAuth2AuthenticationToken token) {
		Integer amount = noteService.doDelete(id);
		return amount.equals(1) ? getList(model) : toItem(id, model);
	}
}

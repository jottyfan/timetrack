package de.jottyfan.timetrack.modules.profile;

import static de.jottyfan.timetrack.db.profile.Tables.T_PROFILE;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.InsertOnDuplicateSetMoreStep;
import org.jooq.Record1;
import org.jooq.SelectConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.timetrack.db.profile.tables.records.TProfileRecord;

/**
 * 
 * @author jotty
 * 
 */
@Repository
public class ProfileRepository {
	private static final Logger LOGGER = LogManager.getLogger(ProfileRepository.class);
	
	@Autowired
	private DSLContext jooq;
	
	public void setTheme(String username, String theme) {
		InsertOnDuplicateSetMoreStep<TProfileRecord> sql = jooq
		// @formatter:off
			.insertInto(T_PROFILE,
					        T_PROFILE.USERNAME,
					        T_PROFILE.THEME)
			.values(username, theme)
			.onConflict(T_PROFILE.USERNAME)
			.doUpdate()
			.set(T_PROFILE.THEME, theme);
		// @formatter:on
		LOGGER.trace(sql.toString());
		sql.execute();
	}
	
	public String getTheme(String username) {
		SelectConditionStep<Record1<String>> sql = jooq
		// @formatter:off
			.select(T_PROFILE.THEME)
			.from(T_PROFILE)
			.where(T_PROFILE.USERNAME.eq(username));
		// @formatter:on
		LOGGER.trace(sql.toString());
		Record1<String> res = sql.fetchOne();
		if (res == null) {
			return "light"; // default
		} else {
			return res.get(T_PROFILE.THEME);
		}
	}
}

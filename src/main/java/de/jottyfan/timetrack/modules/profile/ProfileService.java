package de.jottyfan.timetrack.modules.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * 
 * @author jotty
 * 
 */
@Service
public class ProfileService {
	
	@Autowired
	private ProfileRepository repository;
	
	public void setTheme(String username, String theme) {
		repository.setTheme(username, theme);
	}

	public String getTheme(String username) {
		return username == null ? "light" : repository.getTheme(username);
	}
}

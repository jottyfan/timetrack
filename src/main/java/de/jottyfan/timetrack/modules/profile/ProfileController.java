package de.jottyfan.timetrack.modules.profile;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.jottyfan.timetrack.component.OAuth2Provider;

/**
 * 
 * @author jotty
 * 
 */
@Controller
public class ProfileController {

	@Autowired
	private OAuth2Provider provider;

	@Autowired
	private ProfileService service;
	
	@RequestMapping(value = "/profile/{theme}", method = RequestMethod.POST)
	public String setTheme(@PathVariable String theme) {
		String username = provider.getName();
		service.setTheme(username, theme);
		return "redirect:/";
	}
}

package de.jottyfan.timetrack.modules;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;

import de.jottyfan.timetrack.component.OAuth2Provider;
import de.jottyfan.timetrack.modules.done.DoneBean;
import de.jottyfan.timetrack.modules.done.DoneModel;
import de.jottyfan.timetrack.modules.done.DoneService;
import de.jottyfan.timetrack.modules.done.SummaryBean;
import de.jottyfan.timetrack.modules.profile.ProfileService;
import jakarta.annotation.security.RolesAllowed;
import jakarta.servlet.ServletException;
import jakarta.servlet.http.HttpServletRequest;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class IndexController {
	private static final Logger LOGGER = LogManager.getLogger(IndexController.class);

	@Autowired
	private DoneService doneService;
	
	@Autowired
	private OAuth2Provider provider;

	@Autowired
	private ProfileService profileService;
	
	@GetMapping("/logout")
	public String getLogout(HttpServletRequest request) throws ServletException {
		request.logout();
		return "redirect:/";
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping("/")
	public String getIndex(@ModelAttribute DoneModel doneModel, Model model, OAuth2AuthenticationToken token) {
		String username = provider.getName();
		Duration maxWorkTime = Duration.ofHours(8); // TODO: to the configuration file
		LocalDate day = LocalDate.now();
		List<DoneBean> list = doneService.getList(day, username);
		model.addAttribute("sum", new SummaryBean(list, day, maxWorkTime));
		model.addAttribute("theme", profileService.getTheme(username));
		LOGGER.debug("sum = {}", model.getAttribute("sum"));
		return "public/index";
	}
}

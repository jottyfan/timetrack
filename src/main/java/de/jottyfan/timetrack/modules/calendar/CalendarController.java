package de.jottyfan.timetrack.modules.calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;

import de.jottyfan.timetrack.component.OAuth2Provider;
import de.jottyfan.timetrack.modules.profile.ProfileService;

/**
 * 
 * @author jotty
 *
 */
@Controller
public class CalendarController {

	@Autowired
	private OAuth2Provider provider;
	
	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private CalendarService service;
	
	@GetMapping("/calendar")
	public String getCalendar(Model model) {
		model.addAttribute("events", service.getJsonEvents());
		model.addAttribute("theme", profileService.getTheme(provider.getName()));
		return "/calendar/calendar";
	}
}

package de.jottyfan.timetrack.modules.calendar;

import static de.jottyfan.timetrack.db.done.Tables.T_DONE;
import static de.jottyfan.timetrack.db.done.Tables.T_PROJECT;
import static de.jottyfan.timetrack.db.done.Tables.T_MODULE;
import static de.jottyfan.timetrack.db.done.Tables.T_JOB;
import static de.jottyfan.timetrack.db.done.Tables.T_BILLING;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.Record7;
import org.jooq.SelectJoinStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

/**
 * 
 * @author henkej
 * 
 */
@Repository
public class CalendarDoneRepository {
	private static final Logger LOGGER = LogManager.getLogger(CalendarDoneRepository.class);
	
	@Autowired
	private DSLContext jooq;

	/**
	 * get all already recorded dates from the data base
	 * 
	 * @return the event beans; an empty list at least
	 */
	public List<EventBean> getAllDates() {
		SelectJoinStep<Record7<Integer, LocalDateTime, LocalDateTime, String, String, String, String>> sql = jooq
		// @formatter:off
			.select(T_DONE.PK,
							T_DONE.TIME_FROM,
							T_DONE.TIME_UNTIL,
							T_PROJECT.NAME,
							T_MODULE.NAME,
							T_JOB.NAME,
							T_BILLING.NAME)
			.from(T_DONE)
			.leftJoin(T_PROJECT).on(T_PROJECT.PK.eq(T_DONE.FK_PROJECT))
			.leftJoin(T_MODULE).on(T_MODULE.PK.eq(T_DONE.FK_MODULE))
			.leftJoin(T_JOB).on(T_JOB.PK.eq(T_DONE.FK_JOB))
			.leftJoin(T_BILLING).on(T_BILLING.PK.eq(T_DONE.FK_BILLING));
		// @formatter:on
		LOGGER.trace(sql.toString());
		List<EventBean> list = new ArrayList<>();
		for (Record7<Integer, LocalDateTime, LocalDateTime, String, String, String, String> r : sql.fetch()) {
			String id = String.valueOf(r.get(T_DONE.PK));
			String job = r.get(T_JOB.NAME);
			String project = r.get(T_PROJECT.NAME);
			String module = r.get(T_MODULE.NAME);
			String billing = r.get(T_BILLING.NAME);
			LocalDateTime start = r.get(T_DONE.TIME_FROM);
			LocalDateTime end  = r.get(T_DONE.TIME_UNTIL);
			StringBuilder buf = new StringBuilder();
			buf.append(billing).append(billing == null ? "" : "; ");
			buf.append(job).append(job == null ? "" : " - ");
			buf.append(module).append(module == null ? "" : ": ");
			buf.append(project);
			String title = buf.toString();
			list.add(EventBean.ofEvent(id, title, start, end));
		}
		return list;
	}
}

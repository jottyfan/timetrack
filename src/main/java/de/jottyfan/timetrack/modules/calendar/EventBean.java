package de.jottyfan.timetrack.modules.calendar;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * 
 * @author jotty
 *
 */
public class EventBean implements Serializable {

	private static final long serialVersionUID = 1L;

	private String id;
	private String groupId;
	private Boolean allDay;
	private LocalDateTime start;
	private LocalDateTime end;
	private String startStr;
	private String endStr;
	private String title;
	private String url;
	private final List<String> classNames;
	private Boolean editable;
	private Boolean startEditable;
	private Boolean durationEditable;
	private Boolean resourceEditable;
	private String display; // valid values are: auto, block, list-item, background, inverse-background,
													// none
	private Boolean overlap;
	private String constraint;
	private String backgroundColor;
	private String boderColor;
	private String textColor;
	private String extendedProps;
	private String source;

	public EventBean() {
		classNames = new ArrayList<>();
	}

	/**
	 * create new all day event
	 * 
	 * @param id    the ID of the event
	 * @param title the title of the event
	 * @param start the day of the event
	 * @return the event
	 */
	public static final EventBean ofAllDayEvent(String id, String title, LocalDate start) {
		EventBean bean = new EventBean();
		bean.setId(id);
		bean.setTitle(title);
		bean.setStart(start == null ? null : start.atStartOfDay());
		bean.setAllDay(true);
		return bean;
	}

	/**
	 * create new event
	 * 
	 * @param id    the ID of the event
	 * @param title the title of the event
	 * @param start the datetime of the start
	 * @param end the datetime of the end
	 * @return the event
	 */
	public static final EventBean ofEvent(String id, String title, LocalDateTime start, LocalDateTime end) {
		EventBean bean = new EventBean();
		bean.setId(id);
		bean.setTitle(title);
		bean.setStart(start);
		bean.setEnd(end);
		bean.setAllDay(false);
		return bean;
	}

	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	/**
	 * @param id the id to set
	 */
	public void setId(String id) {
		this.id = id;
	}

	/**
	 * @return the groupId
	 */
	public String getGroupId() {
		return groupId;
	}

	/**
	 * @param groupId the groupId to set
	 */
	public void setGroupId(String groupId) {
		this.groupId = groupId;
	}

	/**
	 * @return the allDay
	 */
	public Boolean getAllDay() {
		return allDay;
	}

	/**
	 * @param allDay the allDay to set
	 */
	public void setAllDay(Boolean allDay) {
		this.allDay = allDay;
	}

	/**
	 * @return the start
	 */
	public LocalDateTime getStart() {
		return start;
	}

	/**
	 * @param start the start to set
	 */
	public void setStart(LocalDateTime start) {
		this.start = start;
	}

	/**
	 * @return the end
	 */
	public LocalDateTime getEnd() {
		return end;
	}

	/**
	 * @param end the end to set
	 */
	public void setEnd(LocalDateTime end) {
		this.end = end;
	}

	/**
	 * @return the startStr
	 */
	public String getStartStr() {
		return startStr;
	}

	/**
	 * @param startStr the startStr to set
	 */
	public void setStartStr(String startStr) {
		this.startStr = startStr;
	}

	/**
	 * @return the endStr
	 */
	public String getEndStr() {
		return endStr;
	}

	/**
	 * @param endStr the endStr to set
	 */
	public void setEndStr(String endStr) {
		this.endStr = endStr;
	}

	/**
	 * @return the title
	 */
	public String getTitle() {
		return title;
	}

	/**
	 * @param title the title to set
	 */
	public void setTitle(String title) {
		this.title = title;
	}

	/**
	 * @return the url
	 */
	public String getUrl() {
		return url;
	}

	/**
	 * @param url the url to set
	 */
	public void setUrl(String url) {
		this.url = url;
	}

	/**
	 * @return the classNames
	 */
	public List<String> getClassNames() {
		return classNames;
	}

	/**
	 * @return the editable
	 */
	public Boolean getEditable() {
		return editable;
	}

	/**
	 * @param editable the editable to set
	 */
	public void setEditable(Boolean editable) {
		this.editable = editable;
	}

	/**
	 * @return the startEditable
	 */
	public Boolean getStartEditable() {
		return startEditable;
	}

	/**
	 * @param startEditable the startEditable to set
	 */
	public void setStartEditable(Boolean startEditable) {
		this.startEditable = startEditable;
	}

	/**
	 * @return the durationEditable
	 */
	public Boolean getDurationEditable() {
		return durationEditable;
	}

	/**
	 * @param durationEditable the durationEditable to set
	 */
	public void setDurationEditable(Boolean durationEditable) {
		this.durationEditable = durationEditable;
	}

	/**
	 * @return the resourceEditable
	 */
	public Boolean getResourceEditable() {
		return resourceEditable;
	}

	/**
	 * @param resourceEditable the resourceEditable to set
	 */
	public void setResourceEditable(Boolean resourceEditable) {
		this.resourceEditable = resourceEditable;
	}

	/**
	 * @return the display
	 */
	public String getDisplay() {
		return display;
	}

	/**
	 * @param display the display to set
	 */
	public void setDisplay(String display) {
		this.display = display;
	}

	/**
	 * @return the overlap
	 */
	public Boolean getOverlap() {
		return overlap;
	}

	/**
	 * @param overlap the overlap to set
	 */
	public void setOverlap(Boolean overlap) {
		this.overlap = overlap;
	}

	/**
	 * @return the constraint
	 */
	public String getConstraint() {
		return constraint;
	}

	/**
	 * @param constraint the constraint to set
	 */
	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}

	/**
	 * @return the backgroundColor
	 */
	public String getBackgroundColor() {
		return backgroundColor;
	}

	/**
	 * @param backgroundColor the backgroundColor to set
	 */
	public void setBackgroundColor(String backgroundColor) {
		this.backgroundColor = backgroundColor;
	}

	/**
	 * @return the boderColor
	 */
	public String getBoderColor() {
		return boderColor;
	}

	/**
	 * @param boderColor the boderColor to set
	 */
	public void setBoderColor(String boderColor) {
		this.boderColor = boderColor;
	}

	/**
	 * @return the textColor
	 */
	public String getTextColor() {
		return textColor;
	}

	/**
	 * @param textColor the textColor to set
	 */
	public void setTextColor(String textColor) {
		this.textColor = textColor;
	}

	/**
	 * @return the extendedProps
	 */
	public String getExtendedProps() {
		return extendedProps;
	}

	/**
	 * @param extendedProps the extendedProps to set
	 */
	public void setExtendedProps(String extendedProps) {
		this.extendedProps = extendedProps;
	}

	/**
	 * @return the source
	 */
	public String getSource() {
		return source;
	}

	/**
	 * @param source the source to set
	 */
	public void setSource(String source) {
		this.source = source;
	}
}

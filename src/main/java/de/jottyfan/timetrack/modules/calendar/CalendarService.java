package de.jottyfan.timetrack.modules.calendar;

import java.time.LocalDate;
import java.time.LocalDateTime;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

/**
 * 
 * @author jotty
 *
 */
@Service
public class CalendarService {
	
	@Autowired
	private CalendarDoneRepository repository;

	// TODO: implement a davical database reader as a repository
	// TODO: implement a caldav client as repository
	// TODO: implement a radicale file reader as repository
	// TODO: create resource management database table for the calendar credentials

	public String getJsonEvents() {
		Gson gson = new GsonBuilder().registerTypeAdapter(LocalDate.class, new LocalDateAdapter())
				.registerTypeAdapter(LocalDateTime.class, new LocalDateTimeAdapter()).create();
		return gson.toJson(repository.getAllDates());
	}

}

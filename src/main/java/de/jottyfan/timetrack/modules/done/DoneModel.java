package de.jottyfan.timetrack.modules.done;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;

import org.springframework.format.annotation.DateTimeFormat;


/**
 * 
 * @author henkej
 *
 */
public class DoneModel implements Serializable {
	private static final long serialVersionUID = 1L;
	
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	private LocalDate day;
	
	public DoneModel() {
		this.day = LocalDate.now();
	}

	public String getDayString() {
		return day == null ? "" : day.format(DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}

	public void setDayString(String day) {
		this.day = day == null ? null : LocalDate.parse(day, DateTimeFormatter.ofPattern("yyyy-MM-dd"));
	}
	
	/**
	 * @return the day
	 */

	public LocalDate getDay() {
		return day;
	}

	/**
	 * @param day the day to set
	 */
	public void setDay(LocalDate day) {
		this.day = day;
	}
}

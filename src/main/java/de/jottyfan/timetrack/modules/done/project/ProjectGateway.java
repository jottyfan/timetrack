package de.jottyfan.timetrack.modules.done.project;

import static de.jottyfan.timetrack.db.done.Tables.T_PROJECT;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertReturningStep;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.timetrack.db.done.tables.records.TProjectRecord;

/**
 * 
 * @author jotty
 *
 */
@Repository
public class ProjectGateway {
	private static final Logger LOGGER = LogManager.getLogger(ProjectGateway.class);
	private final DSLContext jooq;

	public ProjectGateway(@Autowired DSLContext jooq) throws Exception {
		this.jooq = jooq;
	}

	public DSLContext getJooq() {
		return this.jooq;
	}

	public TProjectRecord getProject(Integer id) {
		SelectConditionStep<TProjectRecord> sql = getJooq().selectFrom(T_PROJECT).where(T_PROJECT.PK.eq(id));
		LOGGER.debug(sql.toString());
		return sql.fetchOne();
	}

	public Integer upsert(TProjectRecord bean) {
		return bean.getPk() != null ? update(bean) : insert(bean);
	}

	private Integer insert(TProjectRecord bean) {
		InsertReturningStep<TProjectRecord> sql = getJooq()
		// @formatter:off
			.insertInto(T_PROJECT, 
									T_PROJECT.NAME)
			.values(bean.getName())
			.onConflict(T_PROJECT.NAME)
			.doNothing();
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	private Integer update(TProjectRecord bean) {
		UpdateConditionStep<TProjectRecord> sql = getJooq()
		// @formatter:off
			.update(T_PROJECT)
			.set(T_PROJECT.NAME, bean.getName())
			.where(T_PROJECT.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	public Integer delete(Integer id) {
		DeleteConditionStep<TProjectRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_PROJECT)
			.where(T_PROJECT.PK.eq(id));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}
}

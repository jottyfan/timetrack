package de.jottyfan.timetrack.modules.done.job;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.jottyfan.timetrack.component.OAuth2Provider;
import de.jottyfan.timetrack.db.done.tables.records.TJobRecord;
import de.jottyfan.timetrack.modules.done.DoneController;
import de.jottyfan.timetrack.modules.done.DoneModel;
import de.jottyfan.timetrack.modules.profile.ProfileService;
import jakarta.annotation.security.RolesAllowed;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class JobController {
	@Autowired
	private JobService jobService;

	@Autowired
	private DoneController doneController;
	
	@Autowired
	private OAuth2Provider provider;
	
	@Autowired
	private ProfileService profileService;
	
	@RolesAllowed("timetrack_user")
	@GetMapping("/done/edit/job/{id}")
	public String toJob(@PathVariable Integer id, Model model) {
		String username = provider.getName();
		TJobRecord job = jobService.get(id);
		model.addAttribute("jobBean", job);
		model.addAttribute("theme", profileService.getTheme(username));
		return "done/job";
	}
	
	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/upsert/job", method = RequestMethod.POST)
	public String doUpsert(Model model, @ModelAttribute TJobRecord bean) {
		Integer amount = jobService.doUpsert(bean);
		return amount.equals(1) ? doneController.getList(new DoneModel(), model) : toJob(bean.getPk(), model);
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/add/job", method = RequestMethod.GET)
	public String toAddJob(Model model) {
		return toJob(null, model);
	}

	@RolesAllowed("timetrack_user")
	@GetMapping(value = "/done/delete/job/{id}")
	public String doDeleteJob(@PathVariable Integer id, Model model) {
		Integer amount = jobService.doDelete(id);
		return amount.equals(1) ? doneController.getList(new DoneModel(), model) : toJob(id, model);
	}
}

package de.jottyfan.timetrack.modules.done;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * 
 * @author henkej
 *
 */
public class SummaryBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private final Duration maxDayWorktime;
	private final LocalDate day;
	private final List<DoneBean> list;

	/**
	 * @param list           the list of beans
	 * @param day            the day that this selection refers to; might be the day
	 *                       of the week for weekly sets
	 * @param maxDayWorktime the maximum work time of a day
	 */
	public SummaryBean(List<DoneBean> list, LocalDate day, Duration maxDayWorktime) {
		this.list = list;
		this.day = day;
		this.maxDayWorktime = maxDayWorktime;
	}

	public String toJson() {
		StringBuilder buf = new StringBuilder("{");
		buf.append("\"maxDayWorktime\": \"")
				.append(String.format("%02d:%02d", maxDayWorktime.toHoursPart(), maxDayWorktime.toMinutesPart()));
		buf.append("\", \"schedule\": [");
		boolean first = true;
		for (DoneBean bean : list) {
			if (!first) {
				buf.append(", ");
			} else {
				first = false;
			}
			buf.append(bean.toJson());
		}
		buf.append("]}");
		return buf.toString();
	}

	@Override
	public String toString() {
		StringBuilder buf = new StringBuilder("{");
		buf.append("start=").append(getStart());
		buf.append(",end=").append(getEnd());
		buf.append(",pause=").append(getPause());
		buf.append(",overdue=").append(getOverdue());
		buf.append("}");
		return buf.toString();
	}

	public String getBillingTime(String key) {
		Duration duration = Duration.ZERO;
		for (DoneBean bean : list) {
			if (key == null && bean.getBilling() == null) {
				duration = duration.plus(bean.getTimeDiffDuration());
			} else if (bean.getBilling() != null && bean.getBilling().getShortcut().equals(key)) {
				duration = duration.plus(bean.getTimeDiffDuration());
			}
		}
		int minutes = duration.toMinutesPart();
		int minutesPercent = Float.valueOf((5f / 3f) * minutes).intValue();
		return String.format("%d,%d h", duration.toHoursPart(), minutesPercent);
	}

	/**
	 * @return the start
	 */
	private LocalDateTime getStartTime() {
		LocalDateTime found = day.atTime(23, 59);
		for (DoneBean bean : list) {
			LocalDateTime ldt = bean.getTimeFrom();
			if (ldt != null && ldt.isBefore(found)) {
				found = ldt;
			}
		}
		return found;
	}

	/**
	 * @return the end
	 */
	private LocalDateTime getEndTime() {
		LocalDateTime found = day.atTime(0, 0);
		for (DoneBean bean : list) {
			LocalDateTime ldt = bean.getTimeUntil();
			if (ldt != null && ldt.isAfter(found)) {
				found = ldt;
			}
		}
		return found;
	}

	/**
	 * get sum of break times
	 * 
	 * @return the sum of the break times; 0 at least
	 */
	private Duration getBreakTime() {
		LocalDateTime startTime = getStartTime();
		LocalDateTime endTime = getEndTime();
		Duration totalTime = getTotalTime();
		Duration dayTime = Duration.between(startTime, endTime);
		return dayTime.minus(totalTime);
	}

	/**
	 * @return total time of the day as the summary of all work times of the day
	 */
	private Duration getTotalTime() {
		Duration duration = Duration.ZERO;
		for (DoneBean bean : list) {
			Duration current = bean.getTimeDiffDuration();
			duration = duration.plus(current);
		}
		return duration;
	}

	/**
	 * @return the start time as formatted string
	 */
	public String getStart() {
		LocalDateTime found = getStartTime();
		return String.format("%02d:%02d", found.getHour(), found.getMinute() % 60);
	}

	/**
	 * @return the end time as formatted string
	 */
	public String getEnd() {
		LocalDateTime found = getEndTime();
		return String.format("%02d:%02d", found.getHour(), found.getMinute() % 60);
	}

	/**
	 * @return the total
	 */
	public String getTotal() {
		Duration total = getTotalTime();
		return String.format("%02d:%02d", total.toHours(), total.toMinutesPart());
	}

	/**
	 * @return the pause
	 */
	public String getPause() {
		Duration breakTime = getBreakTime();
		Long hours = breakTime.toHours();
		Integer minutes = breakTime.toMinutesPart();
		hours = hours < 0 ? 0 : hours;
		minutes = minutes < 0 ? 0 : minutes;
		return String.format("%02d:%02d", hours, minutes);
	}

	/**
	 * @return the overdue
	 */
	public String getOverdue() {
		Duration overdue = getTotalTime().minus(maxDayWorktime);
		int minutes = overdue.toMinutesPart();
		int hours = overdue.toHoursPart();
		boolean negative = overdue.isNegative();
		minutes = minutes < 0 ? (-1 * minutes) : minutes;
		hours = hours < 0 ? (-1 * hours) : hours;
		return String.format("%s%02d:%02d", negative ? "-" : "", hours, minutes);
	}
}

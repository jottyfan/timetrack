package de.jottyfan.timetrack.modules.done.module;

import static de.jottyfan.timetrack.db.done.Tables.T_MODULE;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertReturningStep;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.timetrack.db.done.tables.records.TModuleRecord;

/**
 * 
 * @author jotty
 *
 */
@Repository
public class ModuleGateway {
	private static final Logger LOGGER = LogManager.getLogger(ModuleGateway.class);
	private final DSLContext jooq;

	public ModuleGateway(@Autowired DSLContext jooq) throws Exception {
		this.jooq = jooq;
	}

	public DSLContext getJooq() {
		return this.jooq;
	}

	public TModuleRecord getModule(Integer id) {
		SelectConditionStep<TModuleRecord> sql = getJooq().selectFrom(T_MODULE).where(T_MODULE.PK.eq(id));
		LOGGER.debug(sql.toString());
		return sql.fetchOne();
	}

	public Integer upsert(TModuleRecord bean) {
		return bean.getPk() != null ? update(bean) : insert(bean);
	}

	private Integer insert(TModuleRecord bean) {
		InsertReturningStep<TModuleRecord> sql = getJooq()
		// @formatter:off
			.insertInto(T_MODULE, 
									T_MODULE.NAME)
			.values(bean.getName())
			.onConflict(T_MODULE.NAME)
			.doNothing();
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	private Integer update(TModuleRecord bean) {
		UpdateConditionStep<TModuleRecord> sql = getJooq()
		// @formatter:off
			.update(T_MODULE)
			.set(T_MODULE.NAME, bean.getName())
			.where(T_MODULE.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	public Integer delete(Integer id) {
		DeleteConditionStep<TModuleRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_MODULE)
			.where(T_MODULE.PK.eq(id));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}
}

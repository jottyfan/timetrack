package de.jottyfan.timetrack.modules.done.job;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.timetrack.db.done.tables.records.TJobRecord;

@Service
@Transactional(transactionManager = "transactionManager")
public class JobService {
	private static final Logger LOGGER = LogManager.getLogger(JobService.class);

	@Autowired
	private DSLContext dsl;

	public TJobRecord get(Integer id) {
		try {
			return id == null ? new TJobRecord() : new JobGateway(dsl).get(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}

	public Integer doUpsert(TJobRecord bean) {
		try {
			return new JobGateway(dsl).upsert(bean);
		} catch (Exception e) {
			LOGGER.error(e);
			return -1;
		}
	}

	public Integer doDelete(Integer id) {
		try {
			return new JobGateway(dsl).delete(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return -1;
		}
	}
}

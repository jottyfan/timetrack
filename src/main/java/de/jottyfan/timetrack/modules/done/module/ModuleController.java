package de.jottyfan.timetrack.modules.done.module;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.jottyfan.timetrack.component.OAuth2Provider;
import de.jottyfan.timetrack.db.done.tables.records.TModuleRecord;
import de.jottyfan.timetrack.modules.done.DoneController;
import de.jottyfan.timetrack.modules.done.DoneModel;
import de.jottyfan.timetrack.modules.profile.ProfileService;
import jakarta.annotation.security.RolesAllowed;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class ModuleController {

	@Autowired
	private ModuleService moduleService;

	@Autowired
	private DoneController doneController;
	
	@Autowired
	private OAuth2Provider provider;
	
	@Autowired
	private ProfileService profileService;

	@RolesAllowed("timetrack_user")
	@GetMapping("/done/edit/module/{id}")
	public String toModule(@PathVariable Integer id, Model model) {
		String username = provider.getName();
		TModuleRecord module = moduleService.get(id);
		model.addAttribute("moduleBean", module);
		model.addAttribute("theme", profileService.getTheme(username));
		return "done/module";
	}
	
	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/upsert/module", method = RequestMethod.POST)
	public String doUpsert(Model model, @ModelAttribute TModuleRecord bean) {
		Integer amount = moduleService.doUpsert(bean);
		return amount.equals(1) ? doneController.getList(new DoneModel(), model) : toModule(bean.getPk(), model);
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/add/module", method = RequestMethod.GET)
	public String toAddModule(Model model) {
		return toModule(null, model);
	}

	@RolesAllowed("timetrack_user")
	@GetMapping(value = "/done/delete/module/{id}")
	public String doDeleteModule(@PathVariable Integer id, Model model) {
		Integer amount = moduleService.doDelete(id);
		return amount.equals(1) ? doneController.getList(new DoneModel(), model) : toModule(id, model);
	}
}

package de.jottyfan.timetrack.modules.done;

import java.io.Serializable;
import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.time.format.DateTimeFormatter;
import java.util.Date;
import java.util.Map;

import de.jottyfan.timetrack.db.done.tables.records.VBillingRecord;
import de.jottyfan.timetrack.db.done.tables.records.TDoneRecord;
import de.jottyfan.timetrack.db.done.tables.records.VJobRecord;
import de.jottyfan.timetrack.db.done.tables.records.VModuleRecord;
import de.jottyfan.timetrack.db.done.tables.records.VProjectRecord;

/**
 * 
 * @author henkej
 *
 */
public class DoneBean implements Serializable, Comparable<DoneBean> {
	private static final long serialVersionUID = 1L;

	private static final DateTimeFormatter hhmm = DateTimeFormatter.ofPattern("HH:mm");

	private Integer pk;
	private LocalDate day;
	private LocalDateTime timeFrom;
	private LocalDateTime timeUntil;
	private VProjectRecord project;
	private VModuleRecord module;
	private VJobRecord activity;
	private VBillingRecord billing;
	private Integer fkProject;
	private Integer fkModule;
	private Integer fkJob;
	private Integer fkBilling;

	public DoneBean() {
		this.day = null;
	}

	public DoneBean(TDoneRecord r, Map<Integer, VProjectRecord> projectMap, Map<Integer, VModuleRecord> moduleMap,
			Map<Integer, VJobRecord> jobMap, Map<Integer, VBillingRecord> billingMap) {
		this.pk = r.getPk();
		this.day = r.getTimeFrom() != null ? r.getTimeFrom().toLocalDate() : (r.getTimeUntil() == null ? r.getTimeUntil().toLocalDate() : LocalDate.now()); 
		this.timeFrom = r.getTimeFrom();
		this.timeUntil = r.getTimeUntil();
		this.project = projectMap.get(r.getFkProject());
		this.module = moduleMap.get(r.getFkModule());
		this.activity = jobMap.get(r.getFkJob());
		this.billing = billingMap.get(r.getFkBilling());
		this.fkProject = project.getPk();
		this.fkModule = module.getPk();
		this.fkJob = activity.getPk();
		this.fkBilling = billing.getPk();
	}
	
	private final String nullable(Object o, String format) {
		if (o == null) {
			return "null";
		} else if (o instanceof Integer) {
			return String.valueOf(o);
		} else if (o instanceof LocalDateTime) {
			LocalDateTime ldt = (LocalDateTime) o;
			return new StringBuilder("\"").append(ldt.format(DateTimeFormatter.ofPattern(format))).append("\"").toString();
		} else if (o instanceof VBillingRecord) {
			VBillingRecord b = (VBillingRecord) o;
			return new StringBuilder("\"").append(b.getCsskey()).append("\"").toString();
		} else {
			return new StringBuilder("\"").append(o).append("\"").toString();
		}
	}
	
	public final String toJson() {
		StringBuilder buf = new StringBuilder("{");
		buf.append("\"daySlot\": ").append(day == null ? "null" : day.getDayOfWeek().getValue());
		buf.append(", \"from\": ").append(nullable(timeFrom, "HH:mm"));
		buf.append(", \"until\": ").append(nullable(timeUntil, "HH:mm"));
		buf.append(", \"billing\": ").append(nullable(billing, null));
		buf.append("}");
		return buf.toString();
	}

	@Override
	public final String toString() {
		StringBuilder buf = new StringBuilder("DoneBean{");
		buf.append("pk=").append(pk);
		buf.append(",timeFrom=").append(timeFrom == null ? "" : timeFrom.format(DateTimeFormatter.ISO_DATE_TIME));
		buf.append(",timeUntil=").append(timeUntil == null ? "" : timeUntil.format(DateTimeFormatter.ISO_DATE_TIME));
		buf.append(",project=").append(project == null ? "" : project.getName());
		buf.append(",module=").append(module == null ? "" : module.getName());
		buf.append(",activity=").append(activity == null ? "" : activity.getName());
		buf.append(",billing=").append(billing == null ? "" : billing.getName());
		buf.append("}");
		return buf.toString();
	}

	public void setLocalDate(LocalDate date) {
		this.day = date;
	}
	
	/**
	 * set the day of timeFrom and timeUntil, keeping the times
	 * 
	 * @param day the day
	 */
	public void setDay(Date day) {
		if (timeFrom != null) {
			LocalDateTime ldt = timeFrom;
			LocalDate date = day.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			ldt = ldt.withYear(date.getYear()).withMonth(date.getMonthValue()).withDayOfMonth(date.getDayOfMonth());
			timeFrom = ldt;
		}
		if (timeUntil != null) {
			LocalDateTime ldt = timeUntil;
			LocalDate date = day.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
			ldt = ldt.withYear(date.getYear()).withMonth(date.getMonthValue()).withDayOfMonth(date.getDayOfMonth());
			timeUntil = ldt;
		}
	}

	public String getTimeSummary() {
		StringBuilder buf = new StringBuilder();
		if (timeFrom != null) {
			buf.append(timeFrom.format(hhmm));
		}
		if (timeUntil != null) {
			buf.append(" - ");
			buf.append(timeUntil.format(hhmm));
		}
		return buf.toString();
	}

	@Override
	public int compareTo(DoneBean o) {
		return o == null || timeFrom == null || o.getTimeFrom() == null ? 0 : timeFrom.compareTo(o.getTimeFrom());
	}

	public String getTimeNote() {
		LocalDateTime earlier = timeFrom != null ? timeFrom : LocalDateTime.now();
		LocalDateTime later = timeUntil != null ? timeUntil : LocalDateTime.now();
		StringBuilder buf = new StringBuilder();
		if (timeFrom == null) {
			buf.append("     ");
		} else {
			buf.append(String.format("%02d:%02d", earlier.getHour(), earlier.getMinute() % 60));
		}
		buf.append(" - ");
		if (timeUntil == null) {
			buf.append("     ");
		} else {
			buf.append(String.format("%02d:%02d", later.getHour(), later.getMinute() % 60));
		}
		return buf.toString();
	}

	public Duration getTimeDiffDuration() {
		LocalDateTime earlier = timeFrom != null ? timeFrom : LocalDateTime.now();
		LocalDateTime later = timeUntil != null ? timeUntil : LocalDateTime.now();
		return Duration.between(earlier, later);
	}
	
	public String getTimeDiff() {
		Duration diff = getTimeDiffDuration();
		return String.format("%02d:%02d", diff.toHours(), diff.toMinutes() % 60);
	}

	/**
	 * try to find out what date this entry is for; if not found, use the current
	 * date
	 * 
	 * @return a local date
	 */
	public LocalDate getLocalDate() {
		if (day != null) {
			return day;
		} else if (timeFrom != null) {
			return timeFrom.toLocalDate();
		} else if (timeUntil != null) {
			return timeUntil.toLocalDate();
		} else {
			return LocalDate.now();
		}
	}

	/**
	 * get local date time from s
	 * 
	 * @param s   the HH:mm formatted values
	 * @param ldt the date as basic for that datetime, for today one can use
	 *            LocalDateTime.now(); in fact this is set if ldt is null internally
	 * @return the generated datetime
	 */
	public LocalDateTime getLocalDateTimeFromHHmm(String s, LocalDateTime ldt) {
		if (s == null || s.trim().isEmpty()) {
			return null;
		} else if (ldt == null) {
			ldt = LocalDateTime.now();
		}
		String[] hm = s.split(":");
		Integer hours = 0;
		Integer minutes = 0;
		if (hm.length > 0) {
			hours = Integer.valueOf(hm[0]);
		}
		if (hm.length > 1) {
			minutes = Integer.valueOf(hm[1]);
		}
		LocalDate theDay = getLocalDate();
		if (theDay != null) {
			ldt = ldt.withYear(theDay.getYear());
			ldt = ldt.withMonth(theDay.getMonthValue());
			ldt = ldt.withDayOfMonth(theDay.getDayOfMonth());
		}
		ldt = ldt.withHour(hours);
		ldt = ldt.withMinute(minutes);
		ldt = ldt.withSecond(0);
		ldt = ldt.withNano(0);
		return ldt;
	}

	public String getProjectName() {
		return project == null ? "" : project.getName();
	}

	public String getModuleName() {
		return module == null ? "" : module.getName();
	}

	public String getJobName() {
		return activity == null ? "" : activity.getName();
	}

	public String getBillingName() {
		return billing == null ? "" : billing.getName();
	}

	public String getBillingShortcut() {
		return billing == null ? "" : billing.getShortcut();
	}

	public String getBillingCsskey() {
		return billing == null ? "" : billing.getCsskey();
	}

	public String getTimeFromString() {
		return timeFrom == null ? "" : timeFrom.format(hhmm);
	}

	public void setTimeFromString(String s) {
		this.timeFrom = getLocalDateTimeFromHHmm(s, null); // use setDay instead
	}

	public String getTimeUntilString() {
		return timeUntil == null ? "" : timeUntil.format(hhmm);
	}

	public void setTimeUntilString(String s) {
		this.timeUntil = getLocalDateTimeFromHHmm(s, null); // use setDay instead
	}

	public Integer getPk() {
		return this.pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}

	public LocalDateTime getTimeFrom() {
		return timeFrom;
	}

	public void setTimeFrom(LocalDateTime timeFrom) {
		this.timeFrom = timeFrom;
	}

	public LocalDateTime getTimeUntil() {
		return timeUntil;
	}

	public void setTimeUntil(LocalDateTime timeUntil) {
		this.timeUntil = timeUntil;
	}

	public VProjectRecord getProject() {
		return project;
	}

	public void setProject(VProjectRecord project) {
		this.project = project;
		this.fkProject = project != null ? project.getPk() : null;
	}

	public VModuleRecord getModule() {
		return module;
	}

	public void setModule(VModuleRecord module) {
		this.module = module;
		this.fkModule = module != null ? module.getPk() : null;
	}

	public VJobRecord getActivity() {
		return activity;
	}

	public void setActivity(VJobRecord activity) {
		this.activity = activity;
		this.fkJob = activity != null ? activity.getPk() : null;
	}

	/**
	 * @return the billing
	 */
	public VBillingRecord getBilling() {
		return billing;
	}

	/**
	 * @param billing the billing to set
	 */
	public void setBilling(VBillingRecord billing) {
		this.billing = billing;
		this.fkBilling = billing != null ? billing.getPk() : null;
	}

	/**
	 * @return the fkProject
	 */
	public Integer getFkProject() {
		return fkProject;
	}

	/**
	 * @param fkProject the fkProject to set
	 */
	public void setFkProject(Integer fkProject) {
		this.fkProject = fkProject;
	}

	/**
	 * @return the fkModule
	 */
	public Integer getFkModule() {
		return fkModule;
	}

	/**
	 * @param fkModule the fkModule to set
	 */
	public void setFkModule(Integer fkModule) {
		this.fkModule = fkModule;
	}

	/**
	 * @return the fkJob
	 */
	public Integer getFkJob() {
		return fkJob;
	}

	/**
	 * @param fkJob the fkJob to set
	 */
	public void setFkJob(Integer fkJob) {
		this.fkJob = fkJob;
	}

	/**
	 * @return the fkBilling
	 */
	public Integer getFkBilling() {
		return fkBilling;
	}

	/**
	 * @param fkBilling the fkBilling to set
	 */
	public void setFkBilling(Integer fkBilling) {
		this.fkBilling = fkBilling;
	}
}

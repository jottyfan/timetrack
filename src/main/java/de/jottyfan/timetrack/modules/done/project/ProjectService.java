package de.jottyfan.timetrack.modules.done.project;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.timetrack.db.done.tables.records.TProjectRecord;

@Service
@Transactional(transactionManager = "transactionManager")
public class ProjectService {
	private static final Logger LOGGER = LogManager.getLogger(ProjectService.class);

	@Autowired
	private DSLContext dsl;

	public TProjectRecord get(Integer id) {
		try {
			return id == null ? new TProjectRecord() : new ProjectGateway(dsl).getProject(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}

	public Integer doUpsert(TProjectRecord bean) {
		try {
			return new ProjectGateway(dsl).upsert(bean);
		} catch (Exception e) {
			LOGGER.error(e);
			return -1;
		}
	}

	public Integer doDelete(Integer id) {
		try {
			return new ProjectGateway(dsl).delete(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return -1;
		}
	}
}

package de.jottyfan.timetrack.modules.done.job;

import static de.jottyfan.timetrack.db.done.Tables.T_JOB;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertReturningStep;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.timetrack.db.done.tables.records.TJobRecord;

/**
 * 
 * @author jotty
 *
 */
@Repository
public class JobGateway {
	private static final Logger LOGGER = LogManager.getLogger(JobGateway.class);
	private final DSLContext jooq;

	public JobGateway(@Autowired DSLContext jooq) throws Exception {
		this.jooq = jooq;
	}

	public DSLContext getJooq() {
		return this.jooq;
	}

	public TJobRecord get(Integer id) {
		SelectConditionStep<TJobRecord> sql = getJooq().selectFrom(T_JOB).where(T_JOB.PK.eq(id));
		LOGGER.debug(sql.toString());
		return sql.fetchOne();
	}

	public Integer upsert(TJobRecord bean) {
		return bean.getPk() != null ? update(bean) : insert(bean);
	}

	private Integer insert(TJobRecord bean) {
		InsertReturningStep<TJobRecord> sql = getJooq()
		// @formatter:off
			.insertInto(T_JOB, 
									T_JOB.NAME)
			.values(bean.getName())
			.onConflict(T_JOB.NAME)
			.doNothing();
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	private Integer update(TJobRecord bean) {
		UpdateConditionStep<TJobRecord> sql = getJooq()
		// @formatter:off
			.update(T_JOB)
			.set(T_JOB.NAME, bean.getName())
			.where(T_JOB.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	public Integer delete(Integer id) {
		DeleteConditionStep<TJobRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_JOB)
			.where(T_JOB.PK.eq(id));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}
}

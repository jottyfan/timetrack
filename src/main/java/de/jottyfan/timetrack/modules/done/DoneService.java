package de.jottyfan.timetrack.modules.done;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClient;
import org.springframework.security.oauth2.client.OAuth2AuthorizedClientService;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.timetrack.db.done.tables.records.VBillingRecord;
import de.jottyfan.timetrack.db.done.tables.records.VJobRecord;
import de.jottyfan.timetrack.db.done.tables.records.VModuleRecord;
import de.jottyfan.timetrack.db.done.tables.records.VProjectRecord;
import de.jottyfan.timetrack.modules.note.NoteService;

/**
 * 
 * @author henkej
 *
 */
@Service
@Transactional(transactionManager = "transactionManager")
public class DoneService {
	private static final Logger LOGGER = LogManager.getLogger(NoteService.class);

	@Autowired
	private DSLContext dsl;

	@Autowired
	private OAuth2AuthorizedClientService security;

	public String getCurrentUser(OAuth2AuthenticationToken token) {
		if (token != null) {
			OAuth2AuthorizedClient client = security.loadAuthorizedClient(token.getAuthorizedClientRegistrationId(),
					token.getName());
			if (client != null) {
				return client.getPrincipalName();
			} else {
				return "client is null";
			}
		} else {
			return "oauth token is null";
		}
	}

	public List<DoneBean> getList(LocalDate day, String username) {
		try {
			DoneGateway gw = new DoneGateway(dsl);
			Integer userId = gw.getUserId(username);
			if (userId == null) {
				LOGGER.warn("userId of user {} is null", username);
			}
			return gw.getAllOfDay(day, userId);
		} catch (Exception e) {
			LOGGER.error(e);
			return new ArrayList<>();
		}
	}

	public List<DoneBean> getWeek(LocalDate day, String username) {
		try {
			DoneGateway gw = new DoneGateway(dsl);
			Integer userId = gw.getUserId(username);
			if (userId == null) {
				LOGGER.warn("userId of user {} is null", username);
			}
			return gw.getAllOfWeek(day, userId);
		} catch (Exception e) {
			LOGGER.error(e);
			return new ArrayList<>();
		}
	}

	public DoneBean getBean(Integer id) {
		try {
			return new DoneGateway(dsl).getBean(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}

	public List<VProjectRecord> getProjects(boolean includeNull) {
		try {
			return new DoneGateway(dsl).getAllProjects(includeNull);
		} catch (Exception e) {
			LOGGER.error(e);
			return new ArrayList<>();
		}
	}

	public List<VModuleRecord> getModules(boolean includeNull) {
		try {
			return new DoneGateway(dsl).getAllModules(includeNull);
		} catch (Exception e) {
			LOGGER.error(e);
			return new ArrayList<>();
		}
	}

	public List<VJobRecord> getJobs(boolean includeNull) {
		try {
			return new DoneGateway(dsl).getAllJobs(includeNull);
		} catch (Exception e) {
			LOGGER.error(e);
			return new ArrayList<>();
		}
	}

	public List<VBillingRecord> getBillings(boolean includeNull) {
		try {
			return new DoneGateway(dsl).getAllBillings(includeNull);
		} catch (Exception e) {
			LOGGER.error(e);
			return new ArrayList<>();
		}
	}

	public Integer doUpsert(DoneBean bean, String username) {
		try {
			DoneGateway gw = new DoneGateway(dsl);
			Integer userId = gw.getUserId(username);
			return gw.upsert(bean, userId);
		} catch (Exception e) {
			LOGGER.error(e);
			return -1;
		}
	}

	public Integer doDelete(Integer id) {
		try {
			return new DoneGateway(dsl).delete(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return -1;
		}
	}
}

package de.jottyfan.timetrack.modules.done.module;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import de.jottyfan.timetrack.db.done.tables.records.TModuleRecord;

@Service
@Transactional(transactionManager = "transactionManager")
public class ModuleService {
	private static final Logger LOGGER = LogManager.getLogger(ModuleService.class);

	@Autowired
	private DSLContext dsl;

	public TModuleRecord get(Integer id) {
		try {
			return id == null ? new TModuleRecord() : new ModuleGateway(dsl).getModule(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return null;
		}
	}

	public Integer doUpsert(TModuleRecord bean) {
		try {
			return new ModuleGateway(dsl).upsert(bean);
		} catch (Exception e) {
			LOGGER.error(e);
			return -1;
		}
	}

	public Integer doDelete(Integer id) {
		try {
			return new ModuleGateway(dsl).delete(id);
		} catch (Exception e) {
			LOGGER.error(e);
			return -1;
		}
	}
}

package de.jottyfan.timetrack.modules.done.project;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.jottyfan.timetrack.component.OAuth2Provider;
import de.jottyfan.timetrack.db.done.tables.records.TProjectRecord;
import de.jottyfan.timetrack.modules.done.DoneController;
import de.jottyfan.timetrack.modules.done.DoneModel;
import de.jottyfan.timetrack.modules.profile.ProfileService;
import jakarta.annotation.security.RolesAllowed;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class ProjectController {
	@Autowired
	private ProjectService projectService;

	@Autowired
	private DoneController doneController;

	@Autowired
	private OAuth2Provider provider;
	
	@Autowired
	private ProfileService profileService;

	@RolesAllowed("timetrack_user")
	@GetMapping("/done/edit/project/{id}")
	public String toProject(@PathVariable Integer id, Model model) {
		String username = provider.getName();
		TProjectRecord project = projectService.get(id);
		model.addAttribute("projectBean", project);
		model.addAttribute("theme", profileService.getTheme(username));
		return "done/project";
	}
	
	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/upsert/project", method = RequestMethod.POST)
	public String doUpsert(Model model, @ModelAttribute TProjectRecord bean) {
		Integer amount = projectService.doUpsert(bean);
		return amount.equals(1) ? doneController.getList(new DoneModel(), model) : toProject(bean.getPk(), model);
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/add/project", method = RequestMethod.GET)
	public String toAddProject(Model model) {
		return toProject(null, model);
	}

	@RolesAllowed("timetrack_user")
	@GetMapping(value = "/done/delete/project/{id}")
	public String doDeleteProject(@PathVariable Integer id, Model model) {
		Integer amount = projectService.doDelete(id);
		return amount.equals(1) ? doneController.getList(new DoneModel(), model) : toProject(id, model);
	}
}

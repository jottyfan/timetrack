package de.jottyfan.timetrack.modules.done;

import java.time.Duration;
import java.time.LocalDate;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import de.jottyfan.timetrack.component.OAuth2Provider;
import de.jottyfan.timetrack.modules.profile.ProfileService;
import jakarta.annotation.security.RolesAllowed;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class DoneController {

	@Autowired
	private OAuth2Provider provider;
	
	@Autowired
	private ProfileService profileService;

	@Autowired
	private DoneService doneService;

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/list")
	public String getList(@ModelAttribute DoneModel doneModel, Model model) {
		String username = provider.getName();
		Duration maxWorkTime = Duration.ofHours(8); // TODO: to the configuration file
		LocalDate day = doneModel.getDay();
		List<DoneBean> list = doneService.getList(day, username);
		List<DoneBean> week = doneService.getWeek(day, username);
		SummaryBean bean = new SummaryBean(list, day, maxWorkTime);
		SummaryBean weekBean = new SummaryBean(week, day, maxWorkTime);
		model.addAttribute("doneList", list);
		model.addAttribute("doneModel", doneModel);
		model.addAttribute("sum", bean);
		model.addAttribute("schedule", weekBean.toJson());
		model.addAttribute("projectList", doneService.getProjects(false));
		model.addAttribute("moduleList", doneService.getModules(false));
		model.addAttribute("jobList", doneService.getJobs(false));
		model.addAttribute("billingList", doneService.getBillings(false));
		model.addAttribute("theme", profileService.getTheme(username));
		return "done/list";
	}

	@RolesAllowed("timetrack_user")
	@GetMapping("/done/abort/{day}")
	public String abort(@PathVariable String day, Model model) {
		DoneModel doneModel = new DoneModel();
		doneModel.setDayString(day);
		return getList(doneModel, model);
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/add/{day}", method = RequestMethod.GET)
	public String toAdd(@PathVariable @DateTimeFormat(pattern = "yyyy-MM-dd") LocalDate day, Model model) {
		DoneBean bean = new DoneBean();
		bean.setLocalDate(day);
		return toItem(bean, model);
	}

	private String toItem(DoneBean bean, Model model) {
		String username = provider.getName();
		DoneModel doneModel = new DoneModel();
		doneModel.setDay(bean.getLocalDate());
		model.addAttribute("doneBean", bean);
		model.addAttribute("doneModel", doneModel);
		model.addAttribute("projectList", doneService.getProjects(true));
		model.addAttribute("moduleList", doneService.getModules(true));
		model.addAttribute("jobList", doneService.getJobs(true));
		model.addAttribute("billingList", doneService.getBillings(true));
		model.addAttribute("theme", profileService.getTheme(username));
		return "done/item";
	}

	@RolesAllowed("timetrack_user")
	@GetMapping("/done/edit/{id}")
	public String toItem(@PathVariable Integer id, Model model) {
		DoneBean bean = doneService.getBean(id);
		if (bean == null) {
			bean = new DoneBean(); // the add case; typically, only add from today
		}
		return toItem(bean, model);
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/done/upsert", method = RequestMethod.POST)
	public String doUpsert(Model model, @ModelAttribute DoneBean bean) {
		String username = provider.getName();
		Integer amount = doneService.doUpsert(bean, username);
		DoneModel doneModel = new DoneModel();
		doneModel.setDay(bean.getLocalDate());
		return amount.equals(1) ? getList(doneModel, model) : toItem(bean.getPk(), model);
	}

	@RolesAllowed("timetrack_user")
	@GetMapping(value = "/done/delete/{id}")
	public String doDelete(@PathVariable Integer id, Model model) {
		DoneBean bean = doneService.getBean(id);
		Integer amount = doneService.doDelete(id);
		DoneModel doneModel = new DoneModel();
		doneModel.setDay(bean.getLocalDate());
		return amount.equals(1) ? getList(doneModel, model) : toItem(id, model);
	}
}

package de.jottyfan.timetrack.modules.done;

import static de.jottyfan.timetrack.db.done.Tables.T_DONE;
import static de.jottyfan.timetrack.db.done.Tables.V_BILLING;
import static de.jottyfan.timetrack.db.done.Tables.V_JOB;
import static de.jottyfan.timetrack.db.done.Tables.V_MODULE;
import static de.jottyfan.timetrack.db.done.Tables.V_PROJECT;
import static de.jottyfan.timetrack.db.profile.Tables.T_LOGIN;

import java.sql.SQLException;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertValuesStep7;
import org.jooq.Record7;
import org.jooq.Result;
import org.jooq.SelectConditionStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.timetrack.db.done.tables.records.TDoneRecord;
import de.jottyfan.timetrack.db.done.tables.records.VBillingRecord;
import de.jottyfan.timetrack.db.done.tables.records.VJobRecord;
import de.jottyfan.timetrack.db.done.tables.records.VModuleRecord;
import de.jottyfan.timetrack.db.done.tables.records.VProjectRecord;
import de.jottyfan.timetrack.db.profile.tables.records.TLoginRecord;
import de.jottyfan.timetrack.help.LocalDateHelper;

/**
 * 
 * @author jotty
 *
 */
@Repository
public class DoneGateway {
	private static final Logger LOGGER = LogManager.getLogger(DoneGateway.class);
	private final DSLContext jooq;

	public DoneGateway(@Autowired DSLContext jooq) throws Exception {
		this.jooq = jooq;
	}

	public DSLContext getJooq() {
		return this.jooq;
	}

	/**
	 * get the user id of the user with username
	 * 
	 * @param username the name of the user
	 * @return the ID of the user; if not found, null
	 */
	public Integer getUserId(String username) {
		Result<TLoginRecord> r = getJooq().selectFrom(T_LOGIN).where(T_LOGIN.LOGIN.eq(username)).fetch();
		return r == null || r.size() < 1 ? null : r.get(0).getPk();
	}

	/**
	 * get all projects from the database
	 * 
	 * @param includeNull if true, add a null value at the beginning of the result
	 * 
	 * @return a list of found projects
	 * 
	 * @throws DataAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<VProjectRecord> getAllProjects(boolean includeNull)
			throws DataAccessException, ClassNotFoundException, SQLException {
		List<VProjectRecord> list = new ArrayList<>();
		if (includeNull) {
			list.add(new VProjectRecord());
		}
		list.addAll(getJooq().selectFrom(V_PROJECT).orderBy(V_PROJECT.NAME).fetchInto(VProjectRecord.class));
		return list;
	}

	/**
	 * get all modules from the database
	 * 
	 * @param includeNull if true, add a null value at the beginning of the result
	 * 
	 * @return a list of found modules
	 * 
	 * @throws DataAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<VModuleRecord> getAllModules(boolean includeNull)
			throws DataAccessException, ClassNotFoundException, SQLException {
		List<VModuleRecord> list = new ArrayList<>();
		if (includeNull) {
			list.add(new VModuleRecord());
		}
		list.addAll(getJooq().selectFrom(V_MODULE).orderBy(V_MODULE.NAME).fetchInto(VModuleRecord.class));
		return list;
	}

	/**
	 * get all jobs from the database
	 * 
	 * @param includeNull if true, add a null value at the beginning of the result
	 * 
	 * @return a list of found jobs
	 * 
	 * @throws DataAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<VJobRecord> getAllJobs(boolean includeNull)
			throws DataAccessException, ClassNotFoundException, SQLException {
		List<VJobRecord> list = new ArrayList<>();
		if (includeNull) {
			list.add(new VJobRecord());
		}
		list.addAll(getJooq().selectFrom(V_JOB).orderBy(V_JOB.NAME).fetchInto(VJobRecord.class));
		return list;
	}

	/**
	 * get all billings from the database
	 * 
	 * @param includeNull if true, add a null value at the beginning of the result
	 * 
	 * @return a list of found billings
	 * 
	 * @throws DataAccessException
	 * @throws ClassNotFoundException
	 * @throws SQLException
	 */
	public List<VBillingRecord> getAllBillings(boolean includeNull)
			throws DataAccessException, ClassNotFoundException, SQLException {
		List<VBillingRecord> list = new ArrayList<>();
		if (includeNull) {
			list.add(new VBillingRecord());
		}
		list.addAll(getJooq().selectFrom(V_BILLING).orderBy(V_BILLING.NAME).fetchInto(VBillingRecord.class));
		return list;
	}

	/**
	 * generate a project map from the database
	 * 
	 * @return the project map
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	private Map<Integer, VProjectRecord> getProjectMap()
			throws DataAccessException, ClassNotFoundException, SQLException {
		Map<Integer, VProjectRecord> map = new HashMap<>();
		for (VProjectRecord r : getAllProjects(false)) {
			map.put(r.getPk(), r);
		}
		return map;
	}

	/**
	 * generate a module map from the database
	 * 
	 * @return the module map
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	private Map<Integer, VModuleRecord> getModuleMap() throws DataAccessException, ClassNotFoundException, SQLException {
		Map<Integer, VModuleRecord> map = new HashMap<>();
		for (VModuleRecord r : getAllModules(false)) {
			map.put(r.getPk(), r);
		}
		return map;
	}

	/**
	 * generate a job map from the database
	 * 
	 * @return the job map
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	private Map<Integer, VJobRecord> getJobMap() throws DataAccessException, ClassNotFoundException, SQLException {
		Map<Integer, VJobRecord> map = new HashMap<>();
		for (VJobRecord r : getAllJobs(false)) {
			map.put(r.getPk(), r);
		}
		return map;
	}

	/**
	 * generate a billing map from the database
	 * 
	 * @return the billing map
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	private Map<Integer, VBillingRecord> getBillingMap()
			throws DataAccessException, ClassNotFoundException, SQLException {
		Map<Integer, VBillingRecord> map = new HashMap<>();
		for (VBillingRecord r : getAllBillings(false)) {
			map.put(r.getPk(), r);
		}
		return map;
	}

	/**
	 * get list of entries of interval
	 * 
	 * @param start  the lower limit
	 * @param end    the upper limit
	 * @param userId the id of the user
	 * 
	 * @return a list (an empty one at least)
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	private List<DoneBean> getAllOfInterval(LocalDateTime start, LocalDateTime end, Integer userId)
			throws DataAccessException, ClassNotFoundException, SQLException {
		SelectConditionStep<Record7<Integer, LocalDateTime, LocalDateTime, Integer, Integer, Integer, Integer>> sql = getJooq()
		// @formatter:off
			.select(T_DONE.PK,
							T_DONE.TIME_FROM,
							T_DONE.TIME_UNTIL,
							T_DONE.FK_PROJECT,
							T_DONE.FK_MODULE,
							T_DONE.FK_JOB,
							T_DONE.FK_BILLING)
			.from(T_DONE)
			.where(T_DONE.TIME_FROM.between(start, end).or(T_DONE.TIME_FROM.isNull()))
			.and(T_DONE.TIME_UNTIL.between(start, end).or(T_DONE.TIME_UNTIL.isNull()))
			.and(T_DONE.FK_LOGIN.eq(userId == null ? -999999 : userId));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		List<DoneBean> list = new ArrayList<>();
		Map<Integer, VProjectRecord> projectMap = getProjectMap();
		Map<Integer, VModuleRecord> moduleMap = getModuleMap();
		Map<Integer, VJobRecord> jobMap = getJobMap();
		Map<Integer, VBillingRecord> billingMap = getBillingMap();
		for (Record7<Integer, LocalDateTime, LocalDateTime, Integer, Integer, Integer, Integer> r : sql.fetch()) {
			DoneBean bean = new DoneBean();
			bean.setPk(r.get(T_DONE.PK));
			bean.setTimeFrom(r.get(T_DONE.TIME_FROM));
			bean.setTimeUntil(r.get(T_DONE.TIME_UNTIL));
			bean.setLocalDate(bean.getLocalDate());
			bean.setProject(projectMap.get(r.get(T_DONE.FK_PROJECT)));
			bean.setModule(moduleMap.get(r.get(T_DONE.FK_MODULE)));
			bean.setActivity(jobMap.get(r.get(T_DONE.FK_JOB)));
			bean.setBilling(billingMap.get(r.get(T_DONE.FK_BILLING)));
			list.add(bean);
		}
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	/**
	 * get list of entries of day
	 * 
	 * @param day the day
	 * 
	 * @return a list (an empty one at least)
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public List<DoneBean> getAllOfDay(LocalDate day, Integer userId)
			throws DataAccessException, ClassNotFoundException, SQLException {
		LocalDateTime dayStart = day.atStartOfDay();
		LocalDateTime dayEnd = day.atTime(23, 59, 59);
		return getAllOfInterval(dayStart, dayEnd, userId);
	}

	/**
	 * get all entries of the week where day belongs to
	 * 
	 * @param day    the day that the week refers to
	 * @param userId the id of the user
	 * @return a list of done beans; an empty one at least
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public List<DoneBean> getAllOfWeek(LocalDate day, Integer userId)
			throws DataAccessException, ClassNotFoundException, SQLException {
		LocalDate sunday = LocalDateHelper.seekDay(day, DayOfWeek.SUNDAY, true);
		LocalDate saturday = LocalDateHelper.seekDay(day, DayOfWeek.SATURDAY, false);
		return getAllOfInterval(sunday.atStartOfDay(), saturday.atTime(23, 59, 59), userId);
	}

	/**
	 * delete an entry from the database
	 * 
	 * @param pk the id of the entry
	 * @return the number of affected database rows, should be 1
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer delete(Integer pk) {
		DeleteConditionStep<TDoneRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_DONE)
			.where(T_DONE.PK.eq(pk));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * get the done bean of the pk
	 * 
	 * @param pk the ID of the data set
	 * @return the bean if found; null otherwise
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public DoneBean getBean(Integer pk) throws DataAccessException, ClassNotFoundException, SQLException {
		SelectConditionStep<Record7<Integer, LocalDateTime, LocalDateTime, Integer, Integer, Integer, Integer>> sql = getJooq()
		// @formatter:off
			.select(T_DONE.PK,
							T_DONE.TIME_FROM,
							T_DONE.TIME_UNTIL,
							T_DONE.FK_PROJECT,
							T_DONE.FK_MODULE,
							T_DONE.FK_JOB,
							T_DONE.FK_BILLING)
			.from(T_DONE)
			.where(T_DONE.PK.eq(pk));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		Map<Integer, VProjectRecord> projectMap = getProjectMap();
		Map<Integer, VModuleRecord> moduleMap = getModuleMap();
		Map<Integer, VJobRecord> jobMap = getJobMap();
		Map<Integer, VBillingRecord> billingMap = getBillingMap();
		for (Record7<Integer, LocalDateTime, LocalDateTime, Integer, Integer, Integer, Integer> r : sql.fetch()) {
			DoneBean bean = new DoneBean();
			bean.setPk(r.get(T_DONE.PK));
			bean.setTimeFrom(r.get(T_DONE.TIME_FROM));
			bean.setTimeUntil(r.get(T_DONE.TIME_UNTIL));
			bean.setLocalDate(bean.getLocalDate());
			bean.setProject(projectMap.get(r.get(T_DONE.FK_PROJECT)));
			bean.setModule(moduleMap.get(r.get(T_DONE.FK_MODULE)));
			bean.setActivity(jobMap.get(r.get(T_DONE.FK_JOB)));
			bean.setBilling(billingMap.get(r.get(T_DONE.FK_BILLING)));
			return (bean);
		}
		return (null);
	}

	public Integer upsert(DoneBean bean, Integer userId) {
		return bean.getPk() != null ? update(bean) : insert(bean, userId);
	}

	private Integer insert(DoneBean bean, Integer userId) {
		InsertValuesStep7<TDoneRecord, LocalDateTime, LocalDateTime, Integer, Integer, Integer, Integer, Integer> sql = getJooq()
		// @formatter:off
			.insertInto(T_DONE,
					        T_DONE.TIME_FROM,
					        T_DONE.TIME_UNTIL,
					        T_DONE.FK_PROJECT,
					        T_DONE.FK_MODULE,
					        T_DONE.FK_JOB,
					        T_DONE.FK_BILLING,
					        T_DONE.FK_LOGIN)
			.values(bean.getTimeFrom(), bean.getTimeUntil(), bean.getFkProject(), bean.getFkModule(), bean.getFkJob(), bean.getFkBilling(), userId);
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}

	private Integer update(DoneBean bean) {
		UpdateConditionStep<TDoneRecord> sql = getJooq()
		// @formatter:off
			.update(T_DONE)
			.set(T_DONE.TIME_FROM, bean.getTimeFrom())
			.set(T_DONE.TIME_UNTIL, bean.getTimeUntil())
			.set(T_DONE.FK_PROJECT, bean.getFkProject())
			.set(T_DONE.FK_MODULE, bean.getFkModule())
			.set(T_DONE.FK_JOB, bean.getFkJob())
			.set(T_DONE.FK_BILLING, bean.getFkBilling())
			.where(T_DONE.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.debug(sql.toString());
		return sql.execute();
	}
}

package de.jottyfan.timetrack.modules.contact;

import java.io.Serializable;

import de.jottyfan.timetrack.db.contact.enums.EnumContacttype;

/**
 * 
 * @author jotty
 *
 */
public class ContactBean implements Serializable, Comparable<ContactBean> {
	private static final long serialVersionUID = 1L;

	private Integer pk;
	private String forename;
	private String surname;
	private String contact;
	private EnumContacttype type;

	public ContactBean() {
		super();
		this.pk = null;
	}
	
	public ContactBean(Integer pk) {
		super();
		this.pk = pk;
	}

	@Override
	public int compareTo(ContactBean o) {
		return o == null ? 0 : getSortname().compareTo(o.getSortname());
	}

	public String getSortname() {
		StringBuilder buf = new StringBuilder();
		buf.append(surname).append(forename);
		return buf.toString().toLowerCase();
	}

	public String getFullname() {
		StringBuilder buf = new StringBuilder();
		buf.append(forename).append(" ");
		buf.append(surname);
		return buf.toString();
	}

	public String getForename() {
		return forename;
	}

	public void setForename(String forename) {
		this.forename = forename;
	}

	public String getSurname() {
		return surname;
	}

	public void setSurname(String surname) {
		this.surname = surname;
	}

	public String getContact() {
		return contact;
	}

	public void setContact(String contact) {
		this.contact = contact;
	}

	public EnumContacttype getType() {
		return type;
	}

	public void setType(EnumContacttype type) {
		this.type = type;
	}

	public Integer getPk() {
		return pk;
	}

	public void setPk(Integer pk) {
		this.pk = pk;
	}
}

package de.jottyfan.timetrack.modules.contact;

import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.oauth2.client.authentication.OAuth2AuthenticationToken;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import de.jottyfan.timetrack.component.OAuth2Provider;
import de.jottyfan.timetrack.db.contact.enums.EnumContacttype;
import de.jottyfan.timetrack.modules.profile.ProfileService;
import jakarta.annotation.security.RolesAllowed;

/**
 * 
 * @author henkej
 *
 */
@Controller
public class ContactController {

	@Autowired
	private OAuth2Provider provider;

	@Autowired
	private ProfileService profileService;
	
	@Autowired
	private ContactService contactService;

	@ModelAttribute("currentUser")
	@ResponseBody
	public String getCurrentUser(OAuth2AuthenticationToken token) {
		return contactService.getCurrentUser(token);
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/contact/list")
	public String getList(Model model) {
		List<ContactBean> list = contactService.getList();
		model.addAttribute("contactList", list);
		model.addAttribute("theme", profileService.getTheme(provider.getName()));
		return "contact/list";
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/contact/add", method = RequestMethod.GET)
	public String toAdd(Model model) {
		return toItem(null, model);
	}

	@RolesAllowed("timetrack_user")
  @GetMapping("/contact/edit/{id}")
	public String toItem(@PathVariable Integer id, Model model) {
		ContactBean bean = contactService.getBean(id);
		if (bean == null) {
			bean = new ContactBean(); // the add case
		}
		model.addAttribute("contactBean", bean);
		model.addAttribute("types", Arrays.asList(EnumContacttype.values()));
		model.addAttribute("theme", profileService.getTheme(provider.getName()));
		return "contact/item";
	}

	@RolesAllowed("timetrack_user")
	@RequestMapping(value = "/contact/upsert", method = RequestMethod.POST)
	public String doUpsert(Model model, @ModelAttribute ContactBean bean) {
		Integer amount = contactService.doUpsert(bean);
		return amount.equals(1) ? getList(model) : toItem(bean.getPk(), model);
	}

	@RolesAllowed("timetrack_user")
	@GetMapping(value = "/contact/delete/{id}")
	public String doDelete(@PathVariable Integer id, Model model) {
		Integer amount = contactService.doDelete(id);
		return amount.equals(1) ? getList(model) : toItem(id, model);
	}
}

package de.jottyfan.timetrack.modules.contact;

import static de.jottyfan.timetrack.db.contact.Tables.T_CONTACT;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.jooq.DSLContext;
import org.jooq.DeleteConditionStep;
import org.jooq.InsertValuesStep4;
import org.jooq.Record1;
import org.jooq.Record5;
import org.jooq.SelectConditionStep;
import org.jooq.SelectJoinStep;
import org.jooq.UpdateConditionStep;
import org.jooq.exception.DataAccessException;
import org.jooq.impl.DSL;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import de.jottyfan.timetrack.db.contact.enums.EnumContacttype;
import de.jottyfan.timetrack.db.contact.tables.records.TContactRecord;

/**
 * 
 * @author jotty
 *
 */
@Repository
public class ContactRepository {
	private static final Logger LOGGER = LogManager.getLogger(ContactRepository.class);
	private final DSLContext jooq;

	public ContactRepository(@Autowired DSLContext jooq) throws Exception {
		this.jooq = jooq;
	}

	public DSLContext getJooq() {
		return this.jooq;
	}

	/**
	 * get sorted list of contacts
	 * 
	 * @return a list (an empty one at least)
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public List<ContactBean> getAll() throws DataAccessException, ClassNotFoundException, SQLException {
		SelectJoinStep<Record5<Integer, String, String, String, EnumContacttype>> sql = getJooq()
		// @formatter:off
			.select(T_CONTACT.PK,
					    T_CONTACT.FORENAME,
					    T_CONTACT.SURNAME,
					    T_CONTACT.CONTACT,
					    T_CONTACT.TYPE)
			.from(T_CONTACT);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		List<ContactBean> list = new ArrayList<>();
		for (Record5<Integer, String, String, String, EnumContacttype> r : sql.fetch()) {
			ContactBean bean = new ContactBean(r.get(T_CONTACT.PK));
			bean.setForename(r.get(T_CONTACT.FORENAME));
			bean.setSurname(r.get(T_CONTACT.SURNAME));
			bean.setContact(r.get(T_CONTACT.CONTACT));
			bean.setType(r.get(T_CONTACT.TYPE));
			list.add(bean);
		}
		list.sort((o1, o2) -> o1 == null ? 0 : o1.compareTo(o2));
		return list;
	}

	/**
	 * delete a contact from the database
	 * 
	 * @param pk the id of the contact
	 * @return the number of affected database rows, should be 1
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer delete(Integer pk) throws DataAccessException, ClassNotFoundException, SQLException {
		DeleteConditionStep<TContactRecord> sql = getJooq()
		// @formatter:off
			.deleteFrom(T_CONTACT)
			.where(T_CONTACT.PK.eq(pk));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * add a contact to the database
	 *
	 * @param bean the contact information
	 * @return the number of affected database rows, should be 1
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer add(ContactBean bean) throws DataAccessException, ClassNotFoundException, SQLException {
		InsertValuesStep4<TContactRecord, String, String, String, EnumContacttype> sql = getJooq()
		// @formatter:off
			.insertInto(T_CONTACT,
					        T_CONTACT.FORENAME,
									T_CONTACT.SURNAME,
									T_CONTACT.CONTACT,
									T_CONTACT.TYPE)
			.values(bean.getForename(), bean.getSurname(), bean.getContact(), bean.getType());
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * update a contact in the database, referencing its pk
	 * 
	 * @param bean the contact information
	 * @return the number of affected database rows, should be 1
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer update(ContactBean bean) throws DataAccessException, ClassNotFoundException, SQLException {
		UpdateConditionStep<TContactRecord> sql = getJooq()
		// @formatter:off
			.update(T_CONTACT)
			.set(T_CONTACT.FORENAME, bean.getForename())
			.set(T_CONTACT.SURNAME, bean.getSurname())
			.set(T_CONTACT.CONTACT, bean.getContact())
			.set(T_CONTACT.TYPE, bean.getType())
			.where(T_CONTACT.PK.eq(bean.getPk()));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.execute();
	}

	/**
	 * get number of entries in t_contact
	 * 
	 * @return number of entries
	 * @throws SQLException
	 * @throws ClassNotFoundException
	 * @throws DataAccessException
	 */
	public Integer getAmount() throws DataAccessException, ClassNotFoundException, SQLException {
		SelectJoinStep<Record1<Integer>> sql = getJooq()
		// @formatter:off
			.selectCount()
			.from(T_CONTACT);
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		return sql.fetchOne(DSL.count());
	}

	/**
	 * get all enum types
	 * 
	 * @return list of enum types
	 */
	public List<EnumContacttype> getTypes() {
		return new ArrayList<>(Arrays.asList(EnumContacttype.values()));
	}

	public ContactBean getBean(Integer id) {
		SelectConditionStep<Record5<Integer, String, String, String, EnumContacttype>> sql = getJooq()
		// @formatter:off
			.select(T_CONTACT.PK,
					    T_CONTACT.FORENAME,
					    T_CONTACT.SURNAME,
					    T_CONTACT.CONTACT,
					    T_CONTACT.TYPE)
			.from(T_CONTACT)
			.where(T_CONTACT.PK.eq(id));
		// @formatter:on
		LOGGER.debug("{}", sql.toString());
		for (Record5<Integer, String, String, String, EnumContacttype> r : sql.fetch()) {
			ContactBean bean = new ContactBean(r.get(T_CONTACT.PK));
			bean.setForename(r.get(T_CONTACT.FORENAME));
			bean.setSurname(r.get(T_CONTACT.SURNAME));
			bean.setContact(r.get(T_CONTACT.CONTACT));
			bean.setType(r.get(T_CONTACT.TYPE));
			return bean;
		}
		return null;
	}
}

package de.jottyfan.timetrack.component;

import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

/**
 * 
 * @author jotty
 * 
 */
@Component
public class OAuth2Provider {

	/**
	 * get the name of the authenticated user or null if not logged in
	 * 
	 * @return the name or null
	 */
	public String getName() {
		Authentication authentication = SecurityContextHolder.getContext().getAuthentication();
		return authentication == null ? null : authentication.getName();
	}
}

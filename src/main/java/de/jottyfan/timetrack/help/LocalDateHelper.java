package de.jottyfan.timetrack.help;

import java.time.DayOfWeek;
import java.time.LocalDate;

/**
 * 
 * @author henkej
 *
 */
public class LocalDateHelper {

	/**
	 * find the day of the week in relation to day
	 * 
	 * @param day       the day to start the search from
	 * @param dayOfWeek the day of the week that is wanted
	 * @param backwards if true, search before day, if false, search after day
	 * @return the found date
	 */
	public static final LocalDate seekDay(LocalDate day, DayOfWeek dayOfWeek, boolean backwards) {
		LocalDate found = day;
		while (!found.getDayOfWeek().equals(dayOfWeek)) {
			found = backwards ? found.minusDays(1) : found.plusDays(1);
		}
		return found;
	}
}

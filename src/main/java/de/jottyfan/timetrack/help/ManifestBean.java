package de.jottyfan.timetrack.help;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.info.BuildProperties;
import org.springframework.stereotype.Component;

/**
 * 
 * @author henkej
 *
 */
@Component
public class ManifestBean {

	@Value("${server.servlet.context-path:}")
	private String contextPath;

	@Autowired(required = false)
	private BuildProperties buildProperties;

	public String getVersion() {
		StringBuilder buf = new StringBuilder();
		buf.append("Version ");
		buf.append(buildProperties == null ? "?" : buildProperties.getVersion());
		return buf.toString();
	}

	public String getContextPath() {
		return (contextPath != null && !contextPath.isBlank()) ? String.format("/%s/", contextPath) : "/";
	}
}

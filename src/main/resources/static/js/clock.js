'use strict';

/**
 * Coolock Definition
 */
class Clock {
	constructor(size, selector, arrowcolor, scalecolor, backgroundcolor) {

		var canvas = $("<canvas>");
		canvas.attr("id", "clockPanel");
		canvas.attr("width", size);
		canvas.attr("height", size);
		canvas.css("border-radius", "4px");
		canvas.css("background", backgroundcolor);
		canvas[0].getContext("2d").canvas.width = size;
		canvas[0].getContext("2d").canvas.height = size;

		$(selector).append(canvas);
		var ctx = canvas[0].getContext("2d");
		this.ctx = ctx;
		this.size = size;
		this.background = backgroundcolor;
		this.color = arrowcolor;
		this.scalecolor = scalecolor;

		this.redraw();
	}

	drawCircle = function(ctx, x, y, r, w) {
		ctx.strokeStyle = this.background;
		ctx.lineWidth = w;
		ctx.beginPath();
		ctx.arc(x, y, r, 0, 2 * Math.PI);
		ctx.stroke();
	};

	drawArrow = function(ctx, x1, y1, x2, y2, col) {
		ctx.strokeStyle = col;
		ctx.fillStyle = this.background;
		ctx.lineCap = "round";
		ctx.lineWidth = 2;
		ctx.beginPath();
		ctx.moveTo(x1, y1);
		ctx.lineTo(x2, y2);
		ctx.stroke();
	};

	redraw = function() {
		var ctx = this.ctx;
		var color = this.color;
		var size = this.size;
		var halfSize = size / 2;
		var d = new Date();
		var h = d.getHours();
		var m = d.getMinutes();
		var s = d.getSeconds();
		var hoursTotal = (h + m / 60 + s / 3600);

    ctx.clearRect(0, 0, size, size);
    
		ctx.beginPath();
		ctx.fillStyle = this.background;
		ctx.fillRect(0, 0, size, size);

		// small hour pointer
		this.drawArrow(ctx, size / 2 + 0.2 * size * Math.cos(hoursTotal * Math.PI / 6 - Math.PI / 2),
			                  size / 2 + 0.2 * size * Math.sin(hoursTotal * Math.PI / 6 - Math.PI / 2),
			                  halfSize, halfSize, color);

		// huge minute pointer
		this.drawArrow(ctx, size / 2 + 0.4 * size * Math.cos(6 * (m + s / 60) * Math.PI / 180 - Math.PI / 2),
			                  size / 2 + 0.4 * size * Math.sin(6 * (m + s / 60) * Math.PI / 180 - Math.PI / 2),
			                  halfSize, halfSize, color);

		ctx.beginPath();
		ctx.arc(halfSize, halfSize, 0.04 * size, 0, Math.PI * 2);

		$("#clockPanel").attr("title", this.lz(h) + ":" + this.lz(m) + ":" + this.lz(s));

		setTimeout(this.redraw.bind(this), 1000);
	};

	lz = function(d) {
		return (d > 9) ? "" + d : "0" + d;
	}
}
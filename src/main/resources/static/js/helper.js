toggleTheme = function() {
	var oldValue = $("html").attr("data-bs-theme");
	var newValue = oldValue == "dark" ? "light" : "dark";
	$("html").attr("data-bs-theme", newValue);
	var url = "profile/" + newValue;
	$.ajax({
		url: url,
		dataType: 'json',
		type: "POST",
		contentType: 'application/json'
	});
}

resetValue = function(selector, value) {
	$(selector).val(value);
	$(selector).change();
	return false;
}

nowPlus = function(summand) {
	const roundToQuarter = date => new Date(Math.round(date / 9e5) * 9e5);
	var now = roundToQuarter(new Date());
	var wanted = new Date(now);
	wanted.setMinutes(now.getMinutes() + summand);
	return wanted.toLocaleTimeString('de', {
		hour12: false,
		hour: "numeric",
		minute: "numeric"
	});
}

validateTime = function(inputField, okButtonId) {
	var value = inputField.value;
	var regexPattern = /^([01]?[0-9]|2[0-3]):[0-5][0-9]$/;
	var valid = value == "" ? true : regexPattern.test(value);
	if (valid) {
		$(inputField).css("background-color", "#cfc").css("color", "black");
		$("[id='" + okButtonId + "']").removeAttr('disabled');
	} else {
		$(inputField).css("background-color", "#fcc").css("color", "black");
		$("[id='" + okButtonId + "']").attr('disabled', 'disabled');
	}
}
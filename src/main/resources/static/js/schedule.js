class Schedule {
	constructor(selector, width, height) {
		var canvas = $("<canvas>");
		canvas.attr("id", "scheduleCanvas");
		canvas.width(width);
		canvas.height(height);
		canvas.attr("width", width);
		canvas.attr("height", height);
		canvas[0].getContext("2d").canvas.width = parseInt(width);
		canvas[0].getContext("2d").canvas.height = parseInt(height);

		$(selector).append(canvas);
		var ctx = canvas[0].getContext("2d");
		this.ctx = ctx;
		this.drawBackgroundWeek(ctx, width, height);
	}
	
	drawBackgroundDay = function(ctx, width, height, offsetx) {
		var hourHeight = parseInt(height / 24);
		var fs = ["#eeeeee", "#bbbbbb", "#eeeeee", "#bbbbbb", "#eeeeee", "#bbbbbb", 
			        "#faf0fa", "#d9cbd9", "#faf0fa", "#9acbd9", "#cdf0fa", "#9acbd9", 
							"#cdf0fa", "#9acbd9", "#cdf0fa", "#d9cbd9", "#faf0fa", "#d9cbd9",
							"#faf0fa", "#d9cbd9", "#eeeeee", "#bbbbbb", "#eeeeee", "#bbbbbb"];
		for (var i = 0; i < 24; i++) {
			var x = parseInt(offsetx);
			var y = parseInt(i * hourHeight);
			var w = parseInt(width);
			var h = parseInt(hourHeight);
			ctx.fillStyle = fs[i];
			ctx.fillRect(x, y, w, h);
		}
    ctx.lineWidth = 1;
		ctx.strokeStyle = "white";
    ctx.strokeRect(offsetx, 0, width, height);
    ctx.strokeStyle = "black";
    ctx.lineWidth = 2;
    ctx.moveTo(parseInt(offsetx), parseInt(hourHeight * 6));
    ctx.lineTo(parseInt(offsetx + width), parseInt(hourHeight * 6));
    ctx.stroke();
    ctx.moveTo(parseInt(offsetx), parseInt(hourHeight * 12));
    ctx.lineTo(parseInt(offsetx + width), parseInt(hourHeight * 12));
    ctx.stroke();
    ctx.moveTo(parseInt(offsetx), parseInt(hourHeight * 18));
    ctx.lineTo(parseInt(offsetx + width), parseInt(hourHeight * 18));
    ctx.stroke();
	}
	
	drawBackgroundWeek = function(ctx, width, height) {
		var dayWidth = width / 7;
		for (var i = 0; i < 7; i++) {
			this.drawBackgroundDay(ctx, dayWidth, height, i * dayWidth + 1);
		}
	}
	
	time2pixel = function (time, hourHeight) {
		var timeArray = time.split(":");
		var hours = parseInt(timeArray[0]);
		var minutes = parseInt(timeArray[1]);
		var pixels = parseInt((hours + (minutes / 60)) * hourHeight);
		return pixels;
	}
	
	drawSlot = function(ctx, slotNr, from, until, color, fillColor) {
		ctx.strokeStyle = color;
		ctx.fillStyle = fillColor;
		ctx.lineCap = "round";
		ctx.lineWidth = 1;
		var hourHeight = parseInt(parseInt($("#scheduleCanvas").height()) / 24);
		var dayWidth = parseInt(parseInt($("#scheduleCanvas").width()) / 7);
		var x = parseInt(slotNr * dayWidth);
		var y = parseInt(this.time2pixel(from, hourHeight));
		var w = dayWidth;
		var h = parseInt(this.time2pixel(until, hourHeight) - y);
		ctx.beginPath();
		ctx.fillRect(x, y, w, h);
		ctx.strokeRect(x, y, w, h);
	}
}
